# utea-ui

#### 介绍
管理后台脚手架，使用 vue+iview ui

#### 软件架构
管理后台脚手架


#### 安装教程

1.  git下来
2.  进入目录  然后npm i

#### 使用说明

1.  修改服务端请求接口配置，本地看情况 proxy代理
2.  npm run serve

#### 随便搞搞图
![截图](https://images.gitee.com/uploads/images/2020/1120/132818_50be433d_908360.png "屏幕截图.png")
