import axios from '@/libs/api.request'

export const getRouterReq = () => {
  return axios.request({
    url: '/usr/adminMenu/getRouter',
    method: 'get'
  })
}
