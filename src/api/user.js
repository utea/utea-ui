import axios from '@/libs/api.request'

export const login = (data) => {
  return axios.request({
    url: '/usr/login',
    data,
    method: 'post'
  })
}

export const getUserInfo = (token) => {
  return axios.request({
    url: '/usr/adminUsr/getInfo',
    params: {
      token
    },
    method: 'get'
  })
}

export const changePwd = (data) => {
  return axios.request({
    url: '/usr/adminUsr/changePwd',
    data,
    method: 'post'
  })
}

export const logout = (token) => {
  return axios.request({
    url: '/usr/logout',
    method: 'post'
  })
}

export const getUnreadCount = () => {
  return axios.request({
    url: '/usr/message/count',
    method: 'get'
  })
}

export const getMessage = () => {
  return axios.request({
    url: '/usr/message/init',
    method: 'get'
  })
}

export const getContentByMsgId = msg_id => {
  return axios.request({
    url: '/usr/message/content',
    method: 'get',
    params: {
      msg_id
    }
  })
}

export const hasRead = msg_id => {
  return axios.request({
    url: '/usr/message/has_read',
    method: 'post',
    data: {
      msg_id
    }
  })
}

export const removeReaded = msg_id => {
  return axios.request({
    url: '/usr/message/remove_readed',
    method: 'post',
    data: {
      msg_id
    }
  })
}

export const restoreTrash = msg_id => {
  return axios.request({
    url: '/usr/message/restore',
    method: 'post',
    data: {
      msg_id
    }
  })
}
