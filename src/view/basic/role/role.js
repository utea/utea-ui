import axios from '@/libs/api.request'

export const findPage = (data) => {
  return axios.request({
    url: '/usr/adminRole/findPage',
    data,
    method: 'post'
  })
}

export const addAdminRole = (data) => {
  return axios.request({
    url: '/usr/adminRole/addAdminRole',
    data,
    method: 'post'
  })
}

export const updAdminRole = (data) => {
  return axios.request({
    url: '/usr/adminRole/updAdminRole',
    data,
    method: 'post'
  })
}

export const delAdminRole = (data) => {
  return axios.request({
    url: '/usr/adminRole/delAdminRole',
    data,
    method: 'post'
  })
}
export const addAuth = ({ roleId, menuIds }) => {
  return axios.request({
    url: `/usr/adminRole/addAuth/${roleId}`,
    data: menuIds,
    method: 'post'
  })
}
export const adminUsrFindRoleList = (name) => {
  return axios.request({
    url: `/usr/adminRole/adminUsrFindRoleList/${name}`
  })
}
export const findUsrRoleList = (id) => {
  return axios.request({
    url: `/usr/adminRole/findUsrRoleList/${id}`
  })
}
