import axios from '@/libs/api.request'

export const findPage = (data) => {
  return axios.request({
    url: '/task/taskInfo/findPage',
    data,
    method: 'post'
  })
}

export const addTask = (data) => {
  return axios.request({
    url: '/task/taskInfo/addTask',
    data,
    method: 'post'
  })
}

export const updTask = (data) => {
  return axios.request({
    url: '/task/taskInfo/updTask',
    data,
    method: 'post'
  })
}
export const delTask = (id) => {
  return axios.request({
    url: `/task/taskInfo/delTask/${id}`
  })
}
