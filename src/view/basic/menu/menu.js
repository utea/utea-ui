import axios from '@/libs/api.request'

export const getMenuTree = () => {
  return axios.request({
    url: '/usr/adminMenu/findTreeDate',
    method: 'get'
  })
}

export const addMenu = (data) => {
  return axios.request({
    url: '/usr/adminMenu/addMenu',
    data: data,
    method: 'post'
  })
}

export const updMenu = (data) => {
  return axios.request({
    url: '/usr/adminMenu/updMenu',
    data: data,
    method: 'post'
  })
}

export const delMenu = (id) => {
  return axios.request({
    url: `/usr/adminMenu/delMenu/${id}`,
    method: 'get'
  })
}

export const findRoleMenu = (id) => {
  return axios.request({
    url: `/usr/adminMenu/findRoleMenu/${id}`,
    method: 'get'
  })
}
