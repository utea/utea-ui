import axios from '@/libs/api.request'

export const findPage = (data) => {
  return axios.request({
    url: '/usr/adminUsr/findPage',
    data,
    method: 'post'
  })
}
export const addAdminUsr = (data) => {
  return axios.request({
    url: '/usr/adminUsr/addAdminUsr',
    data,
    method: 'post'
  })
}
export const updAdminUsr = (data) => {
  return axios.request({
    url: '/usr/adminUsr/updAdminUsr',
    data,
    method: 'post'
  })
}
export const delAdminUsr = (id) => {
  return axios.request({
    url: `/usr/adminUsr/delAdminUsr/${id}`,
    method: 'get'
  })
}
export const authRole = ({ usrId, roleIds }) => {
  return axios.request({
    url: `/usr/adminUsr/authRole/${usrId}`,
    data: roleIds,
    method: 'post'
  })
}
