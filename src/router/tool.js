import Main from '@/components/main'
import { lazyLoadingCop } from '@/libs/tools'

// 构建菜单
export const loadMenu = (menuList) => {
  // console.log(menuList)
  return new Promise((resolve, reject) => {
    let dynamicRoute = {
      path: '/',
      name: 'index',
	  component: Main,
      meta: {
        hideRoot: true, // 根节点是否隐藏
        hideInBread: true
      },
      children: [...formatMenu(menuList)]
    }
    resolve(dynamicRoute)
  })
}

// 格式化菜单
export const formatMenu = (list) => {
  let res = []
  list.forEach(item => {
    let obj = {
      path: item.path,
      name: item.name
    }
    obj.meta = item.meta
    if (item.component) {
      // 这里的data应为 /notice/notice.vue 类似的数据，对应的是src/view/下的本地文件
      obj.component = lazyLoadingCop(item.component)
    } else {
      		obj.component = Main
    }
    if (item.children) {
      obj.children = formatMenu(item.children)
    }
    res.push(obj)
  })
  return res
}
