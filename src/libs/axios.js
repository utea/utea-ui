import axios from 'axios'
import store from '@/store'
import router from '../router'
import { sortParams } from './tools'
import { Message } from 'view-design'
import md5 from 'js-md5'

const appSecret = '123i89wu0sduuihdfniosu981uo0ufj'

const addErrorLog = errorInfo => {
  const { statusText, status, request: { responseURL } } = errorInfo
  let info = {
    type: 'ajax',
    code: status,
    mes: statusText,
    url: responseURL
  }
  if (!responseURL.includes('save_error_logger')) store.dispatch('addErrorLog', info)
}

class HttpRequest {
  constructor (baseUrl = baseURL) {
    this.baseUrl = baseUrl
    this.queue = {}
  }
  getInsideConfig () {
    const config = {
      baseURL: this.baseUrl,
      headers: {
        //
      }
    }
    return config
  }
  destroy (url) {
    delete this.queue[url]
    if (!Object.keys(this.queue).length) {
      // Spin.hide()
    }
  }
  interceptors (instance, url) {
    // 请求拦截
    instance.interceptors.request.use(config => {
      // 添加全局的loading...
      if (!Object.keys(this.queue).length) {
        // Spin.show() // 不建议开启，因为界面不友好
      }
      let token = store.state.user.token
      if (token) {
        config.headers.token = token
      }
      config.headers.appId = 'sign1'
      config.headers.timestamp = Date.parse(new Date()) / 1000
      config.headers.timeoutExpress = 180
      if (config.method === 'post') {
        config.headers.sign = md5(appSecret + config.url + sortParams(config.data) + config.headers.timestamp + config.headers.timeoutExpress).toString()
      } else {
        config.headers.sign = md5(appSecret + config.url + config.headers.timestamp + config.headers.timeoutExpress).toString()
      }
      this.queue[url] = true
      return config
    }, error => {
      return Promise.reject(error)
    })
    // 响应拦截
    instance.interceptors.response.use(res => {
      this.destroy(url)
      const data = res.data
      if (data.code === 401) {
        store.commit('setToken', '')
        Message.error('登陆失效，请重新登陆')
        router.push({ name: 'login' })
        Promise.reject(data.msg)
      }
      if (res.headers.token) {
        store.commit('setToken', res.headers.token)
      }
      return res.data
    }, error => {
      this.destroy(url)
      let errorInfo = error.response
      if (!errorInfo) {
        const { request: { statusText, status }, config } = JSON.parse(JSON.stringify(error))
        errorInfo = {
          statusText,
          status,
          request: { responseURL: config.url }
        }
      }
      addErrorLog(errorInfo)
      return Promise.reject(error)
    })
  }
  request (options) {
    const instance = axios.create()
    options = Object.assign(this.getInsideConfig(), options)
    this.interceptors(instance, options.url)
    return instance(options)
  }
}
export default HttpRequest
